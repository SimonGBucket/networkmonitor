def splitResults(fileName):
    f = open(fileName, 'r')
    lines = f.readlines()
    f.close()

    splitLines = map(lambda x: x.split(','), lines)
    
    res = {}
    for line in splitLines:
        if (res.get(line[1]) == None):
            res[line[1]] = []
        res[line[1]].append(line)
    
    for key, value in res.items():
        with open(fileName + "'{}'.csv".format(key), 'w') as f:
            txt = ''.join(map(lambda x: ','.join(x), value))
            f.write(txt)

splitResults('monitorResults2.csv')