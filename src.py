import threading
import time
import socket
from typing import List, Optional
from collections import namedtuple
from networkMap import NetworkMap
from socketBinder import ConnectionInterface, interfaceMultiPing

PingResults = namedtuple('PingResults', ['time', 'target', 'numberOfPackets', 'min', 'max', 'average', 'packetLoss', 'networkDevices'])

class PingCommand():

    def __init__(self, target : str, numberOfPackets : int = 4):
        self.__target = target
        self.__numberOfPackets = numberOfPackets

    def getTarget(self) -> str:
        return self.__target

    def run(self, networkMap : NetworkMap) -> PingResults:
        try:
            # Move from possible URL to ip
            target = socket.gethostbyname(self.__target)
            results = []
            for _ in range(self.__numberOfPackets):
                inter = interfaceMultiPing(
                    [target],
                    5, 
                    networkMap.getConnectionInterface()
                )
                if (not inter):
                    return None
                results.append(inter[0].get(target, None))
        except socket.gaierror:
            results = [None] * self.__numberOfPackets

        packetLoss = results.count(None) / self.__numberOfPackets

        results = list(filter(None, results))

        if (results == []):
            minPing, maxPing, avgPing = [-1] * 3
        else:
            minPing = min(results)
            maxPing = max(results)
            avgPing = sum(results) / len(results)


        return PingResults(
            time.time(),
            self.__target,
            self.__numberOfPackets,
            minPing,
            maxPing,
            avgPing,
            packetLoss,
            networkMap.getNetworkDevices()
        )

class NetworkMonitor():

    def __log(self, msg : str):
        print('{}    NetworkMonitor ({}):\t{}'.format(
                time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                self.__connInterface.value,
                msg
            )
        )

    def __init__(
        self,
        pingCommands : List[PingCommand],
        connInterface : ConnectionInterface,
        interval : int = 5,
        out : Optional[str] = None):
        # Assign variables
        self.__pingCommands = pingCommands
        self.__pingInterval = interval
        self.__connInterface = connInterface
        self.__timer : threading.Timer = None
        self.__running = False
        self.__outFile = None
        self.__networkMapThread = None

        # Open outfile for appending
        if (out):
            self.__outFile = open(out, 'a+')

        # Add a network map
        self.__networkMap = NetworkMap(self.__connInterface)

        self.__log('Successfully initialized NetworkMonitor.')

    def __timerFunction(self):
        self.__setupTimer()
        self.__executePingCommands()
    
    def __setupTimer(self):
        self.__timer = threading.Timer(self.__pingInterval, self.__timerFunction)
        self.__timer.start()

    def startMonitor(self):
        if (self.__running):
            self.__log("NetworkMonitor already running")
            return

        # Run NetworkMap and wait for initial initialization
        self.__networkMapThread = threading.Thread(target=self.__networkMap.run)
        self.__networkMapThread.start()
        while (not(self.__networkMap.isInitialized())):
            self.__log('Waiting for NetworkMap initialization')
            time.sleep(5)
        self.__networkMapThread.join()


        self.__setupTimer()
        self.__running = True
    
    def stopMonitor(self):
        self.__log('Terminating ...')
        if (self.__timer):
            self.__timer.cancel()
            self.__timer = None
        if (self.__outFile):
            self.__outFile.close()
            self.__outFile = None

        self.__networkMap.stop()
        
        self.__running = False
        self.__log('Successfully terminated.')

    def __executePingCommands(self):
        pingResults = []

        for pingCommand in self.__pingCommands:
            result = pingCommand.run(self.__networkMap)
            if (not result):
                self.__log('Error: Connection for {} not found.'.format(self.__connInterface.value))
            else:
                pingResults.append(result)

        self.__log('Successfully executed all ping commands.')
        
        if (self.__outFile):
            self.__outFile.write(
                '\n'.join(map(lambda x: ','.join(map(str, x)), pingResults)) + '\n'
            )
            self.__outFile.flush()
        



if __name__ == '__main__':
    commands = list(map(PingCommand, ['google.com', 'volkskrant.nl', 'bing.com']))
    connInterface = ConnectionInterface.ETHERNET
    monitors = []
    for connInterface in ConnectionInterface:
        monitors.append(NetworkMonitor(
            commands,
            connInterface,
            interval=6,
            out='{}monitorResults.csv'.format(connInterface.value))
        )
    threads = []
    try:
        for m in monitors:
            thread = threading.Thread(target=m.startMonitor)
            threads.append(thread)
            thread.start()
        while True:
            time.sleep(0.1)
    except KeyboardInterrupt:
        for t, m in zip(threads, monitors):
            t.join()
            m.stopMonitor()
        