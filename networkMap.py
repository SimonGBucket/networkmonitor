from typing import List, Tuple, Dict
from collections import namedtuple
import multiping
import re
import subprocess
import socket
import uuid
import time
import threading
import sys
import os
import netifaces

from socketBinder import ConnectionInterface, interfaceMultiPing, getInterfaceIpMap

NetworkDevice = namedtuple('NetworkDevice', ['ip', 'physical', 'name', 'ping'])

class NetworkMap():
    
    def __log(self, msg : str):
        print('{}    NetworkMap ({}):\t{}'.format(
                time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()),
                self.__connInterface.value,
                msg
            )
        )

    def __init__(self, connInterface : ConnectionInterface, refreshRate : int = 300):
        self.__refreshRate = refreshRate
        self.__connInterface = connInterface
        self.__networkDevices = None
        self.__isRunning = False
        self.__timer : threading.Timer = None
        self.__updateStartTime = None

    @staticmethod
    def __getPhysicalFromARP() -> Dict[str, str]:
        pid = subprocess.Popen(['arp', '-a'], stdout=subprocess.PIPE)
        res = pid.communicate()[0]
        txt : str = str(res, 'utf-8')
        if sys.platform == 'win32':
            REGEX_STRING = r'(192.168.1.[\d]+)[\s]+([^\s-]+-[\S]+)'
        elif sys.platform == 'linux':
            REGEX_STRING = r'.+? \((192.168.1.[\d]{1,3})\) at ([\da-f:]{17})'
        matches = re.findall(REGEX_STRING, txt)

        return dict(matches)

    def isInitialized(self) -> bool:
        return self.__networkDevices != None
    
    def getConnectionInterface(self) -> ConnectionInterface:
        return self.__connInterface

    def run(self) -> None:
        if (self.__isRunning):
            self.__log('Error: NetworkMap is already running.')
            return
        self.__isRunning = True
        self.__updateNetworkMap()
    
    def stop(self) -> None:
        self.__log('Terminating...')
        if (self.__timer):
            self.__timer.cancel()
            self.__timer = None
        
        self.__isRunning = False

        self.__log('Successfully terminated.')
        
    def __updateNetworkMap(self) -> None:
        self.__updateStartTime = time.time()

        self.__log('Updating NetworkMap...')
        try:
            # Addresses ignore broadcast address
            ip = getInterfaceIpMap().get(self.__connInterface.value, None)
            if (not ip):
                pingResults = None
            else:
                pingAddresses = [ip[:ip.rfind('.') + 1] + str(x) for x in range(1, 255)]
                pingResults = interfaceMultiPing(pingAddresses, 5, self.__connInterface)
                if (pingResults):
                    pingResults : Dict[str, float] = pingResults[0]
        except multiping.MultiPingError:
            self.__log('PermissionError: Please run with sudo')
            os._exit(-1)
        if (pingResults):
            self.__log('Ping results received.')
            physicalAddresses = NetworkMap.__getPhysicalFromARP()
            self.__log('Physical addresses received')

            # Add localhost to physical addresses
            hostName = socket.gethostname()
            localIp = socket.gethostbyname(hostName)
            macAddress = hex(uuid.getnode())[2:]
            macAddress = '-'.join(macAddress[i:i + 2] for i in range(0, len(macAddress), 2))

            physicalAddresses[localIp] = macAddress
            
            newNetworkDevices = []
            for pingResult in pingResults.items():
                physAddress = physicalAddresses.get(pingResult[0], None)

                hostName : str = None
                try:
                    res : Tuple[str, List[str], List[str]] = socket.gethostbyaddr(pingResult[0])
                    hostName = res[0]
                except socket.herror as _:
                    self.__log('No associated hostname found for address {}'.format(pingResult[0]))
                
                device = NetworkDevice(
                    pingResult[0],
                    physAddress,
                    hostName,
                    pingResult[1]
                )
                newNetworkDevices.append(device)
            # Sort based on ip
            newNetworkDevices = sorted(newNetworkDevices, key=lambda x: int(x.ip[x.ip.rfind('.') + 1:]))    

            for device in newNetworkDevices:
                self.__log('{}, {}, {}, {}'.format(device.ip, device.physical, device.name, device.ping))

            self.__networkDevices = newNetworkDevices
        else:
            self.__networkDevices = []

        # Reinitialize the timer
        self.__timer = threading.Timer(self.__refreshRate, self.__updateNetworkMap)
        self.__timer.start()

        self.__log("Finished updating. Time taken: {} s".format(time.time() - self.__updateStartTime))
        self.__updateStartTime = None

    def getNetworkDevices(self) -> List[NetworkDevice]:
        if (self.__networkDevices is None):
            return []
        else:
            return list(self.__networkDevices)