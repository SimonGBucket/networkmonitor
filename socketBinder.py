import socket
import sys
import os
import subprocess
import netifaces
import operator
import re
import enum
from multiping import MultiPing, MultiPingError

class ConnectionInterface(enum.Enum):
    WIFI = 'wlan0'
    ETHERNET = 'eth0'

def getInterfaceIpMap():
    if (sys.platform == 'win32'):
        RE_EXPRESSIONS = map(
            lambda x: (x[0], r'{}:.*?(?:IPv4 Address[. ]*?: ([0-9.]+)|Media State)'.format(x[1])),
            [('eth0', 'Ethernet'), ('wlan0', 'Wi-Fi')]
        )
        
        pid = subprocess.Popen(['ipconfig'], stdout=subprocess.PIPE)
        res = pid.communicate()[0]
        txt : str = str(res, 'utf-8')
        
        inter = map(
            lambda z: (z[0], z[1][0]),
            filter(
                lambda y: y[1] and y[1][0],
                map(
                    lambda x: (x[0], re.findall(x[1], txt, re.DOTALL)),
                    RE_EXPRESSIONS
                )
            )
        )
        return dict(inter)
    elif (sys.platform == 'linux'):
        return dict(map(operator.itemgetter(1, 0), netifaces.gateways()[netifaces.AF_INET]))

def bindSocket(connInterface : ConnectionInterface):
    try:
        sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_RAW, proto=socket.IPPROTO_ICMP)
    except PermissionError:
        print('PermissionError: Please run with sudo')
        os._exit(-1)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 131072)

    if (sys.platform == 'win32'):
        ipMap = getInterfaceIpMap()
        if (connInterface.value not in ipMap):
            return None
        
        sock.bind((ipMap[connInterface.value], 0))
    elif (sys.platform == 'linux'):
        SO_BINDTODEVICE = 25
        sock.setsockopt(socket.SOL_SOCKET, SO_BINDTODEVICE, connInterface.value.encode())

    return sock

def interfaceMultiPing(dest_addrs, timeout, connInterface : ConnectionInterface, retry = 0, ignore_lookup_errors = False):
    """
    Copied and modified from:
    https://github.com/romana/multi-ping/blob/master/multiping/__init__.py
    """
    retry = int(retry)
    if retry < 0:
        retry = 0

    timeout = float(timeout)
    if timeout < 0.1:
        raise MultiPingError("Timeout < 0.1 seconds not allowed")

    retry_timeout = float(timeout) / (retry + 1)
    if retry_timeout < 0.1:
        raise MultiPingError("Time between ping retries < 0.1 seconds")

    sock = bindSocket(connInterface)
    if (not sock):
        return None

    try:
        mp = MultiPing(dest_addrs, sock, ignore_lookup_errors=ignore_lookup_errors)
    except Exception as e:
        print(dest_addrs)
        raise

    results = {}
    retry_count = 0
    while retry_count <= retry:
        # Send a batch of pings
        mp.send()
        single_results, no_results = mp.receive(retry_timeout)
        # Add the results from the last sending of pings to the overall results
        results.update(single_results)
        if not no_results:
            # No addresses left? We are done.
            break
        retry_count += 1

    return results, no_results

if (__name__ == '__main__'):
    print(interfaceMultiPing(['google.com', 'vk.nl'], 5, ConnectionInterface.ETHERNET))