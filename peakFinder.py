from typing import Iterable, Dict, Tuple, List

import numpy as np
import time
import re
import operator
import itertools
import datetime
import statistics
from bokeh.plotting import figure, output_file, show
from bokeh.palettes import Dark2_5 as bokehColors

class DataGraph:

    def __init__(self, outputFile = 'plot.html'):
        self.__color = itertools.cycle(bokehColors)
        # Configure the output file
        output_file(outputFile)

        # Create the figure
        self.__figure = figure(
        title="NetworkMonitor Scatter Plot",
        x_axis_label='date', x_axis_type="datetime",
        y_axis_label='ping (s)', y_axis_type="linear",
        output_backend="webgl")

    def show(self):
        show(self.__figure)
    
    def addPlot(self, name, x, y):
        self.__figure.scatter(x, y, legend=name, size=5, color=next(self.__color))

def simpleDataFromCSV(fileName : str):
    with open(fileName, 'r', encoding='utf-8') as f:
        data = f.readlines()
    
    itemGetter = operator.itemgetter(1, 0, 5)
    processedData = sorted(map(
        itemGetter,
        map(lambda x: x.split(','), data)
    ))
    groupedData = itertools.groupby(processedData, operator.itemgetter(0))

    dateF = lambda x: datetime.datetime.fromtimestamp(float(x))
    return list(map(lambda x: (x[0], list(map(
        lambda y: (y[0], dateF(y[1]), float(y[2])),
        x[1]))), groupedData))

def filterData(data, ignoreWithinNStdDev = 1, filterNoConnnections = True):
    # Filter out -1 for no connections
    relevantData = list(filter(lambda x: x[2] != -1, data))

    # Compute median and population standard deviation
    median = statistics.median(map(operator.itemgetter(2), relevantData))
    pStdev = statistics.pstdev(map(operator.itemgetter(2), relevantData))

    if (filterNoConnnections):
        dataToFilter = relevantData
    else:
        dataToFilter = data
    n = ignoreWithinNStdDev
    return list(filter(
        lambda x: x[2] == -1 or x[2] < median - n * pStdev or x[2] > median + n * pStdev,
        dataToFilter
    ))
    


if __name__ == '__main__':
    dataMap = simpleDataFromCSV('wlan0monitorResults.csv')
    dataGraph = DataGraph('wlan.html')
    n = 5
    filterNoConnnections = False
    for key, data in dataMap:
        # Give some information regarding the number of data points
        # left after filtering a variable number of pstddev
        m = map(len, map(lambda x: filterData(data, x, filterNoConnnections), range(1, n + 1)))
        print(key, *zip(range(1,n), m))


        filteredData = filterData(data, n, filterNoConnnections)
        x, y = zip(*map(operator.itemgetter(1,2), filteredData))
        dataGraph.addPlot(filteredData[0][0], x, y)
    dataGraph.show()
        